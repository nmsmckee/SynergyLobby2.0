package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import net.synergyserver.synergylobby.TheHunt;
import org.bukkit.command.CommandSender;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.aggregation.Projection;
import org.mongodb.morphia.aggregation.Sort;

import java.util.Iterator;
import java.util.LinkedHashMap;

@CommandDeclaration(
        commandName = "leaderboard",
        aliases = {"top", "lb"},
        permission = "syn.the-hunt.leaderboard",
        usage = "/thehunt leaderboard",
        description = "Displays the top 10 people in \"The Hunt\".",
        maxArgs = 0,
        parentCommandName = "thehunt"
)
public class TheHuntLeaderboardCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        Datastore ds = MongoDB.getInstance().getDatastore();

        // Get the top 10 WorldGroupProfiles
        Iterator<LobbyWorldGroupProfile> wgps = ds.createAggregation(LobbyWorldGroupProfile.class)
                .match(
                        ds.createQuery(LobbyWorldGroupProfile.class)
                                .field("n").equal(SynergyLobby.getWorldGroupName())
                                .field("fe").exists()
                                .field("fe").notEqual(null)
                )
                .project(
                        Projection.projection("pid"),
                        Projection.projection("ec",
                                Projection.projection("$size", "fe")
                        )
                )
                .sort(
                        Sort.descending("ec")
                )
                .limit(10)
                .aggregate(LobbyWorldGroupProfile.class, MongoDB.getAggregationOptions());


        LinkedHashMap<String, Integer> foundEggs = new LinkedHashMap<>();

        // Store the results
        while (wgps.hasNext()) {
            LobbyWorldGroupProfile wgp = wgps.next();
            foundEggs.put(PlayerUtil.getName(wgp.getPlayerID()), wgp.getFoundEggsCount());
        }

        // Send the header
        sender.sendMessage(Message.format("commands.the_hunt.leaderboard.header"));

        // Send the top 10
        int totalEggs = TheHunt.getInstance().getEggCount();
        int i = 1;
        for (String name : foundEggs.keySet()) {
            sender.sendMessage(Message.format("commands.the_hunt.leaderboard.item", i, name, foundEggs.get(name), totalEggs));

            // Increment the index
            i++;
        }
        return true;
    }
}
