package net.synergyserver.synergylobby.commands;

import net.synergyserver.synergycore.commands.CommandDeclaration;
import net.synergyserver.synergycore.commands.CommandFlags;
import net.synergyserver.synergycore.commands.SenderType;
import net.synergyserver.synergycore.commands.SubCommand;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.MinecraftProfile;
import net.synergyserver.synergycore.utils.PlayerUtil;
import net.synergyserver.synergylobby.LobbyWorldGroupProfile;
import net.synergyserver.synergylobby.SynergyLobby;
import net.synergyserver.synergylobby.TheHunt;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandDeclaration(
        commandName = "progress",
        aliases = "check",
        permission = "syn.the-hunt.progress",
        usage = "/thehunt progress [player]",
        description = "Checks the progress of yourself or another player in \"The Hunt\".",
        maxArgs = 1,
        parentCommandName = "thehunt"
)
public class TheHuntProgressCommand extends SubCommand {

    @Override
    public boolean execute(CommandSender sender, String[] args, CommandFlags flags) {
        boolean isSelf = false;
        UUID pID;
        MinecraftProfile mcp;

        if (args.length == 0) {
            // If args.length is 0 then that means that the sender is required to be a player checking their own playtime
            if (!SenderType.getSenderType(sender).equals(SenderType.PLAYER)) {
                sender.sendMessage(Message.get("commands.error.sender_type_requires_player"));
                return false;
            }

            isSelf = true;
            // Get the UUID of the sender
            pID = ((Player) sender).getUniqueId();
        } else {
            // Get the referenced player
            pID = PlayerUtil.getUUID(args[0], true, sender.hasPermission("vanish.see"));

            // If no player was found then give the sender an error message
            if (pID == null) {
                sender.sendMessage(Message.format("commands.error.player_not_found", args[0]));
                return false;
            }
        }

        String pName = PlayerUtil.getName(pID);

        if (PlayerUtil.isOnline(pID)) {
            mcp = PlayerUtil.getProfile(pID);
        } else {
            mcp = DataManager.getInstance().getPartialDataEntity(MinecraftProfile.class, pID, "wp");
        }

        // Check if the player has data for the Lobby worldgroup
        if (!mcp.hasWorldGroupProfile(SynergyLobby.getWorldGroupName())) {
            // If the player specified is the sender, then give them a special message
            if (isSelf) {
                sender.sendMessage(Message.format("commands.the_hunt.progress.game_info.no_data_self"));
                return true;
            }

            sender.sendMessage(Message.format("commands.the_hunt.progress.game_info.no_data_other", pName));
            return true;
        }

        LobbyWorldGroupProfile wgp = mcp.getPartialWorldGroupProfile(LobbyWorldGroupProfile.class, SynergyLobby.getWorldGroupName(), "fe");
        int foundEggs = wgp.getFoundEggs().size();
        int totalEggs = TheHunt.getInstance().getEggCount();

        // If the player specified is the sender, then give them special feedback
        if (isSelf) {
            sender.sendMessage(Message.format("commands.the_hunt.progress.game_info.self", foundEggs, totalEggs));
            return true;
        } else {
            sender.sendMessage(Message.format("commands.the_hunt.progress.game_info.other", pName, foundEggs, totalEggs));
            return true;
        }
    }
}
