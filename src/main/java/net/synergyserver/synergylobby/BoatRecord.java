package net.synergyserver.synergylobby;

import net.synergyserver.synergycore.database.DataEntity;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Indexed;
import org.mongodb.morphia.annotations.Property;

import java.util.UUID;

/**
 * Represents a player's record for the boat minigame.
 */
@Entity(value = "boatrecords")
public class BoatRecord implements DataEntity {

    @Id
    private ObjectId brID;
    @Property("pid")
    private UUID pID;
    @Property("c")
    private long created;
    @Indexed
    @Property("t")
    private long time;

    /**
     * Required constructor for Morphia to work.
     */
    public BoatRecord() {}

    /**
     * Creates a new <code>BoatRecord</code> with the given time.
     *
     * @param pID The UUID of the player who created this record.
     * @param created The time that this <code>BoatRecord</code> was created.
     * @param time The amount of time it took the player to complete the course.
     */
    public BoatRecord(UUID pID, long created, long time) {
        this.brID = new ObjectId();
        this.pID = pID;
        this.created = created;
        this.time = time;
    }

    @Override
    public ObjectId getID() {
        return brID;
    }

    /**
     * Gets the ID of the player who created this <code>BoatRecord</code>.
     *
     * @return The ID of the player of this <code>BoatRecord</code>
     */
    public UUID getPlayerID() {
        return pID;
    }

    /**
     * Gets the time that this <code>BoatRecord</code> was created.
     *
     * @return The creation date of this <code>BoatRecord</code>.
     */
    public long getCreated() {
        return created;
    }

    /**
     * Gets the time, in milliseconds, that it took the player to complete the course.
     *
     * @return The time of this <code>BoatRecord</code>.
     */
    public long getTime() {
        return time;
    }
}
