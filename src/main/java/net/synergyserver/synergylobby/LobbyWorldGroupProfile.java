package net.synergyserver.synergylobby;

import net.synergyserver.synergycore.database.DataManager;
import net.synergyserver.synergycore.profiles.WorldGroupProfile;
import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Property;
import org.mongodb.morphia.annotations.Transient;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Represents the profile of a player in the Lobby world group.
 */
@Entity(value = "worldgroupprofiles")
public class LobbyWorldGroupProfile extends WorldGroupProfile {

    @Property("br")
    private Set<ObjectId> boatRecordIds;
    @Property("fe")
    private Set<String> foundEggs;
    @Property("ec")
    private int foundEggsCount;

    @Transient
    private DataManager dm = DataManager.getInstance();
    @Transient
    private long boatStart;
    @Transient
    private long boatEnd;

    /**
     * Required constructor for Morphia to work.
     */
    public LobbyWorldGroupProfile() {}

    /**
     * Creates a new <code>LobbyWorldGroupProfile</code> with the given parameters.
     *
     * @param wgp The <code>WorldGroupProfile</code> to extend.
     */
    public LobbyWorldGroupProfile(WorldGroupProfile wgp) {
        super(wgp.getWorldGroupName(), wgp.getPlayerID(), wgp.getLastLogIn(), wgp.getLastLogOut(),
                wgp.getLastLocation(), wgp.getPrefID());

        this.boatRecordIds = new HashSet<>();
        this.foundEggs = new HashSet<>();
    }

    /**
     * Gets the IDs of <code>BoatRecord</code>s of this profile.
     *
     * @return The list of <code>BoatRecord</code> IDs of this profile.
     */
    public Set<ObjectId> getBoatRecordIds() {
        if (boatRecordIds == null) {
            return new HashSet<>();
        }
        return boatRecordIds;
    }

    /**
     * Gets the list of all <code>BoatRecords</code> of this profile.
     *
     * @return All <code>BoatRecords</code> of this profile.
     */
    public List<BoatRecord> getBoatRecords() {
        return dm.getDataEntities(BoatRecord.class, boatRecordIds);
    }

    /**
     * Adds a <code>BoatRecord</code> to the database and this profile if it's one of the top 5 scores by this player.
     *
     * @param boatRecord The <code>BoatRecord</code> to add.
     * @return True if the <code>BoatRecord</code> was stored in the database.
     */
    public boolean addBoatRecord(BoatRecord boatRecord) {
        if (boatRecordIds == null) {
            boatRecordIds = new HashSet<>();
        }

        // If the player has less than 5 records saved, just add the new one
        if (boatRecordIds.size() < 5) {
            boatRecordIds.add(boatRecord.getID());
            dm.updateField(this, LobbyWorldGroupProfile.class, "br", boatRecordIds);
            dm.saveDataEntity(boatRecord);
            return true;
        }

        // Otherwise, find the worst record of this player to delete
        BoatRecord worstRecord = boatRecord;
        for (BoatRecord record : getBoatRecords()) {
            if (record.getTime() > worstRecord.getTime()) {
                worstRecord = record;
            }
        }

        // If the worst record is the one given, return false
        if (worstRecord.equals(boatRecord)) {
            return false;
        }

        // Otherwise add the new record
        boatRecordIds.remove(worstRecord.getID());
        boatRecordIds.add(boatRecord.getID());
        dm.updateField(this, LobbyWorldGroupProfile.class, "br", boatRecordIds);

        // Save the record in the database and delete the old one
        dm.saveDataEntity(boatRecord);
        dm.deleteDataEntity(worstRecord);
        return true;
    }

    /**
     * Gets the best <code>BoatRecord</code> of this profile by its time if it exists.
     *
     * @return The best <code>BoatRecord</code>, or null if none exist.
     */
    public BoatRecord getBestBoatRecord() {
        BoatRecord bestRecord = null;
        for (BoatRecord record : getBoatRecords()) {
            if (bestRecord == null || record.getTime() <= bestRecord.getTime()) {
                bestRecord = record;
            }
        }
        return bestRecord;
    }

    /**
     * Gets the last start time of the boat minigame.
     *
     * @return The last boat start time.
     */
    public long getBoatStart() {
        return boatStart;
    }

    /**
     * Sets the last start time of the boat minigame.
     *
     * @param boatStart The new boat start time.
     */
    public void setBoatStart(long boatStart) {
        this.boatStart = boatStart;
    }

    /**
     * Gets the last end time of the boat minigame.
     *
     * @return The last boat end time.
     */
    public long getBoatEnd() {
        return boatEnd;
    }

    /**
     * Sets the last end time of the boat minigame.
     *
     * @param boatEnd The new boat end time.
     */
    public void setBoatEnd(long boatEnd) {
        this.boatEnd = boatEnd;
    }

    /**
     * Gets whether this player is currently playing the boat minigame.
     *
     * @return True if this player is currently boating.
     */
    public boolean isBoating() {
        return boatStart > boatEnd;
    }

    /**
     * Gets the set of egg IDs of eggs that this player has found.
     *
     * @return The set of found eggs.
     */
    public Set<String> getFoundEggs() {
        if (foundEggs == null) {
            foundEggs = new HashSet<>();
        }

        return foundEggs;
    }

    /**
     * Sets the set of egg IDs of eggs that this player has found.
     *
     * @param foundEggs The new set of found eggs.
     */
    public void setFoundEggs(Set<String> foundEggs) {
        this.foundEggs = foundEggs;
        dm.updateField(this, LobbyWorldGroupProfile.class, "fe", foundEggs);
    }

    /**
     * Adds an egg ID to the list of found eggs by this player.
     *
     * @param eggID The egg ID to add as found.
     * @return True if the egg wasn't already found.
     */
    public void addFoundEgg(String eggID) {
        if (foundEggs == null) {
            foundEggs = new HashSet<>();
        }

        foundEggs.add(eggID);
        dm.updateField(this, LobbyWorldGroupProfile.class, "fe", foundEggs);
    }

    /**
     * Checks if this player has found the egg with the given ID.
     *
     * @param eggID The egg ID to check.
     * @return True if the player has found the egg.
     */
    public boolean hasFoundEgg(String eggID) {
        if (foundEggs == null || foundEggs.isEmpty()) {
            return false;
        }
        return foundEggs.contains(eggID);
    }

    /**
     * Gets the number of eggs this player has found. Only populated by Morphia when
     * using this as a temporary object for returning sorted data for /thehunt top.
     *
     * @return The number of eggs this player has found.
     */
    public int getFoundEggsCount() {
        return foundEggsCount;
    }
}
