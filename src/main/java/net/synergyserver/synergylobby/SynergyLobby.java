package net.synergyserver.synergylobby;

import net.synergyserver.synergycore.SynergyCore;
import net.synergyserver.synergycore.SynergyPlugin;
import net.synergyserver.synergycore.commands.CommandManager;
import net.synergyserver.synergycore.configs.Message;
import net.synergyserver.synergycore.configs.PluginConfig;
import net.synergyserver.synergycore.database.MongoDB;
import net.synergyserver.synergycore.settings.SettingManager;
import net.synergyserver.synergycore.utils.BukkitUtil;
import net.synergyserver.synergylobby.commands.BoatCommand;
import net.synergyserver.synergylobby.commands.ParkourCommand;
import net.synergyserver.synergylobby.commands.TheHuntCommand;
import net.synergyserver.synergylobby.commands.TheHuntLeaderboardCommand;
import net.synergyserver.synergylobby.commands.TheHuntProgressCommand;
import net.synergyserver.synergylobby.commands.TheHuntResetCommand;
import net.synergyserver.synergylobby.listeners.SynergyListener;
import net.synergyserver.synergylobby.listeners.TheHuntListener;
import net.synergyserver.synergylobby.listeners.WorldListener;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The main class for SynergyLobby.
 */
public class SynergyLobby extends JavaPlugin implements SynergyPlugin {

    private static HashMap<Location, String> portals;

    @Override
    public void onEnable() {
        getLogger().info("Hooking into SynergyCore...");
        SynergyCore.getPlugin().registerSynergyPlugin(this);
    }

    public void postLoad() {
        getLogger().info("Loading portals...");
        portals = new HashMap<>();
        Map<String, Object> configPortals = PluginConfig.getConfig(SynergyLobby.getPlugin()).getConfigurationSection("portals").getValues(false);
        for (String key : configPortals.keySet()) {
            portals.put(BukkitUtil.blockLocationFromString(configPortals.get(key).toString()), key);
        }
    }

    public void onReload() {
        // Reload portals
        portals = new HashMap<>();
        Map<String, Object> configPortals = PluginConfig.getConfig(SynergyLobby.getPlugin()).getConfigurationSection("portals").getValues(false);
        for (String key : configPortals.keySet()) {
            portals.put(BukkitUtil.blockLocationFromString(configPortals.get(key).toString()), key);
        }
    }

    @Override
    public void onDisable() {
        getLogger().info("Saving plugin configuration.");
        PluginConfig.getInstance().save(this);

        getLogger().info("Saving messages.");
        Message.getInstance().save(this);
    }

    public void registerCommands() {
        CommandManager cm = CommandManager.getInstance();

        cm.registerMainCommand(this, BoatCommand.class);
        cm.registerMainCommand(this, TheHuntCommand.class);
        cm.registerSubCommand(this, TheHuntProgressCommand.class);
        cm.registerSubCommand(this, TheHuntLeaderboardCommand.class);
        cm.registerSubCommand(this, TheHuntResetCommand.class);
        cm.registerMainCommand(this, ParkourCommand.class);
    }

    public void registerSettings() {
        SettingManager sm = SettingManager.getInstance();
    }

    public void registerListeners() {
        PluginManager pm = Bukkit.getPluginManager();

        pm.registerEvents(new SynergyListener(), this);
        pm.registerEvents(new TheHuntListener(), this);
        pm.registerEvents(new WorldListener(), this);
        // RIP /boat
        //pm.registerEvents(new BoatListener(), this);
    }

    public void mapClasses() {
        MongoDB db = MongoDB.getInstance();
        db.mapClasses(
                LobbyWorldGroupProfile.class
        );
    }

    /**
     * Convenience method for getting this plugin.
     *
     * @return The object representing this plugin.
     */
    public static SynergyLobby getPlugin() {
        return (SynergyLobby) getProvidingPlugin(SynergyLobby.class);
    }

    public String[] getAliases() {
        return new String[]{getName(), "SynLobby", "Lobby", "SynSpawn", "Spawn"};
    }

    /**
     * Gets the list of worlds that this plugin handles.
     *
     * @return The world group of this plugin.
     */
    public static List<String> getWorldGroup() {
        return PluginConfig.getConfig(getPlugin()).getStringList("world_group.worlds");
    }

    /**
     * Gets the name of the world group that this plugin handles.
     *
     * @return The world group name of this plugin.
     */
    public static String getWorldGroupName() {
        return PluginConfig.getConfig(getPlugin()).getString("world_group.name");
    }

    /**
     * Gets the portals of Lobby.
     *
     * @return The HashMap containing block locations and their destinations.
     */
    public static HashMap<Location, String> getPortals() {
        return portals;
    }
}